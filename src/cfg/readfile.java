/**
 * 
 */
package cfg;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Scanner;

import libs.servers;

/**
 * @author pangeranjack.4nr
 *
 */
public class readfile {
	
	private static String PathFile = "";
	private static String PathFile1 = "";
	private static String PathPO = "";
	private static String PathPO1 = "";
	
	private static String PathCust = "";
	private static String PathCust1 = "";
	private static String PathMenu = "";
	private static String PathMenu1 = "";
	
	private static String PathImages = "";
	private static String PathImages1 = "";
	
	private static String data = null, datacfg = null, datamenu = null, datacust=null;
	private static String vals = "", valscfg = "", valsmenu = "", valscust="";
	private static String vals1 = "", valscfg1 = "", valsmenu1 ="", valscust1="";
	private static File file;
	private static Scanner sc;
	private static String line;
	private static URL url;
	private static BufferedReader in;
	
	public static String readFileAsString()throws Exception 
	  { 
		PathFile = servers.getHostname().toString() + "app/config/cfgDB.txt";
		PathFile1 = servers.getHostname().toString() + "app/config/jar/libs/dbmysql/config/cfgDB.txt";
//		System.out.println(PathFile1);
//		System.exit(0);
//		file = new File(PathFile); 
//	    if(!file.exists()) {
//	    	file = new File(PathFile1);
//	    	sc = new Scanner(file); 
//	    }else{
//	    	sc = new Scanner(file);
//	    }
		url = new URL(PathFile1);
		in = new BufferedReader(new InputStreamReader(url.openStream()));
		
	    while ((line = in.readLine()) != null){ 
	    	datacfg = line;
	    	valscfg = datacfg.substring(datacfg.lastIndexOf(" = ") + 2).replace(" ", "");
	    	 if(!valscfg.equals(" ") || !valscfg.equals("")) valscfg1 += valscfg.concat(" ");
	    	 else continue;
	    }
	    in.close();
//	    System.out.println(vals1.replace("null", ""));
	    return valscfg1.replace("null", "");
	  } 
	
	public static String readFileImages()throws Exception 
	  { 
		PathImages = servers.getHostname().toString() + "app/config/images.txt";
		PathImages1 = servers.getHostname().toString() + "app/config/camera/images/images.txt";
//		file = new File(PathImages); 
//	    if(!file.exists()) {
//	    	file = new File(PathImages1);
//	    	sc = new Scanner(file); 
//	    }else{
//	    	sc = new Scanner(file);
//	    }
		url = new URL(PathImages1);
		in = new BufferedReader(new InputStreamReader(url.openStream()));
		
	    while ((line = in.readLine()) != null){ 
	    	data = line;
	    }
	    in.close();
///	    System.out.println(vals1.replace("null", ""));
	    return data;
	  } 
	
	public static String readFilePO()throws Exception 
	  { 
		PathPO = servers.getHostname().toString() + "app/config/po.txt";
		PathPO1 = servers.getHostname().toString() + "app/config/jar/libs/dbmysql/config/po.txt";
//		file = new File(PathPO); 
//	    if(!file.exists()) {
//	    	file = new File(PathPO1);
//	    	sc = new Scanner(file); 
//	    }else{
//	    	sc = new Scanner(file);
//	    }
		url = new URL(PathPO1);
		in = new BufferedReader(new InputStreamReader(url.openStream()));
		
	    while ((line = in.readLine()) != null){ 
	    	 data = line;
	    	 vals = data.substring(data.lastIndexOf(" = ") + 2).replace(" ", "");
	    	 if(!vals.equals(" ") || !vals.equals("")) vals1 += vals.replace(":", "=").replace(",", ", ").replace("AND", " AND ").concat("@@");
	    	 else continue;
	    }
	    in.close();
///	    System.out.println(vals1.replace("null", ""));
	    return vals1.replace("null", "");
	  } 

	public static String readFileMenu()throws Exception 
	  { 
		PathMenu = servers.getHostname().toString() + "app/config/menu.txt";
		PathMenu1 = servers.getHostname().toString() + "app/config/jar/libs/dbmysql/config/menu.txt";
//		file = new File(PathMenu); 
//	    if(!file.exists()) {
//	    	file = new File(PathMenu1);
//	    	sc = new Scanner(file); 
//	    }else{
//	    	sc = new Scanner(file);
//	    }
		url = new URL(PathMenu1);
		in = new BufferedReader(new InputStreamReader(url.openStream()));
		
	    while ((line = in.readLine()) != null){ 
	    	datamenu = line;
	    	valsmenu = datamenu.substring(datamenu.lastIndexOf(" = ") + 2).replace(" ", "");
	    	 if(!valsmenu.equals(" ") || !valsmenu.equals("")) valsmenu1 += valsmenu.replace(":", "=").replace(",", ", ").replace("AND", " AND ").concat("@@");
	    	 else continue;
	    }
	    in.close();
	    
	    return valsmenu1.replace("null", "");
	  } 
	
	public static String readFileCust()throws Exception 
	  { 
		PathCust = servers.getHostname().toString() + "app/config/cust.txt";
		PathCust1 = servers.getHostname().toString() + "app/config/jar/libs/dbmysql/config/cust.txt";
//		file = new File(PathCust); 
//	    if(!file.exists()) {
//	    	file = new File(PathCust1);
//	    	sc = new Scanner(file); 
//	    }else{
//	    	sc = new Scanner(file);
//	    }
		url = new URL(PathCust1);
		in = new BufferedReader(new InputStreamReader(url.openStream()));
		
	    while ((line = in.readLine()) != null){ 
	    	datacust = line;
	    	valscust = datacust.substring(datacust.lastIndexOf(" = ") + 2).replace(" ", "");
	    	 if(!valscust.equals(" ") || !valscust.equals("")) valscust1 += valscust.replace(":", "=").replace(",", ", ").replace("AND", " AND ").concat("@@");
	    	 else continue;
	    }
//	    sc.close();
	    in.close();
///	    System.out.println(vals1.replace("null", ""));
	    return valscust1.replace("null", "");
	  } 
}

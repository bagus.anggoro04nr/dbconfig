/**
 * @author pangeran.jack@4nr
 *
 */
package cfg;

import java.sql.*;

import javax.sql.*;

public class dbmysql {
	
	private static Connection connect;
	
	private static String datatxt;
	private static String[] vals;
	private static String[] db;
	private static int num;
	
    private static String USERNAME;
    private static String PASSWORD;
    private static String DRIVER;
    private static String URL;
	   
	public void dbmysqls(int a){
		num = a;
	}
	
	public static Connection koneksi() throws Exception{
		datatxt = readfile.readFileAsString();
		vals = datatxt.split("\\s+");
		db = vals[3].split(",");
//		System.out.println(vals[0]);
//		System.out.println(vals[1]);
//		System.out.println(db[num]);
		URL = vals[0] + db[num];// + "?profileSQL=true&useUnicode=yes&characterEncoding=UTF-8";
		USERNAME = vals[1];
		PASSWORD = vals[2];
//		System.out.println(URL);
		DRIVER = "com.mysql.cj.jdbc.Driver";
		Class.forName(DRIVER);
		connect = DriverManager.getConnection(URL, USERNAME, PASSWORD);
		
		return connect;
	}
	


}

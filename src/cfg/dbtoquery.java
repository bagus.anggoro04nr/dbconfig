/**
 * 
 */
package cfg;

import java.sql.SQLException;
import java.util.Arrays;

import documents.Create_Documents;
import libs.pathfile;
import libs.setgetdate;
import libs.users;

/**
 * @author pangeranjack.4nr
 *
 */
public class dbtoquery extends dbquery{
	
	private static String datamenu = "", datapo = "", datacust="";
	private static String[] tablesDatas, joinParamsDatas ;
	private static String orderParamsDatas = "", groupByParamsDatas = "", whereparams = "";
	private static String[] vals;
	private static Object[] querys;
	private static Object retrns="", datareports="";
	private static int types;
	private static int conns, numbers, modal;

	
	public static Object getLogin(dbtoquery obj) throws Exception
	{
		tablesDatas = new String[]{"users"};
		joinParamsDatas = new String[]{""};
		orderParamsDatas = "null";
		groupByParamsDatas = "null";
		whereparams = "emails='" + users.getEmails() + "' AND password='" + users.getPass() + "'";
		querys = new Object[]{" SELECT * FROM ", ""};
				
		retrns = obj.dbquerysdata(0, tablesDatas, joinParamsDatas, orderParamsDatas, whereparams, groupByParamsDatas, querys[0].toString(), querys[1].toString(), "1", "");
		return retrns;

	}

	private static String[] getSelectMenu() throws Exception{

		datamenu = readfile.readFileMenu();
		return datamenu.split("@@");
		

	}
	
	private static String[] fromFilePO() throws Exception{
		
		datapo = readfile.readFilePO();
		return datapo.split("@@");
		
	}
	
	private static String[] fromFileCust() throws Exception{
		
		datacust = readfile.readFileCust();
		return datacust.split("@@");
		
	}
	
	public static Object showTableData(dbtoquery obj, String typeSelect, String idModal) throws SQLException, Exception{
		
		if(typeSelect.contains("menu")) vals = getSelectMenu();
		else if(typeSelect.contains("po")) vals = fromFilePO();
		else if(typeSelect.contains("cust")) vals = fromFileCust();
		
		modal = searchNumFromText(vals, idModal);
		
		types = vals.length - 1;
		conns = Integer.valueOf(vals[types - 1]);
		
		querys = (Object[]) obj.dbquerystype(0, vals[0], vals[1], vals[modal], vals[types]);

		tablesDatas = new String[]{querys[0].toString()};
		joinParamsDatas = new String[]{querys[5].toString()};
		orderParamsDatas = querys[7].toString();
		groupByParamsDatas = querys[6].toString();
		whereparams = querys[4].toString();
		if(idModal.matches("reports")) pathfile.setPathFile(querys[8].toString());
		
		
		retrns = obj.dbquerysdata(conns, tablesDatas, joinParamsDatas, orderParamsDatas, whereparams, groupByParamsDatas, querys[1].toString(), querys[2].toString(), "null", idModal);
		return retrns;
		
	}

//	public String qryInsert(String jenis, int conn, String table, String paramstable, String mode, String type, String params, String whereparam, int limit) throws Exception{
//
//		
//		data.dbquerys("INSERT", conn, table, paramstable, mode, type, params, whereparam, limit);
//
//		return data.qryInsert();
//
//	}

//	public String qryDelete(String jenis, int conn, String table, String param, String mode, String type, String params, String whereparam, int limit) throws SQLException, Exception{
//
//		
//		data.dbquerys("UPDATE", conn, table, param, mode, type, params, whereparam,  limit);
//
//		return data.qryDelete();
//
//	}
	
	private static int searchNumFromText(String[] data, String searchText){
		
		for(int i=0; i<data.length; i++){
			if(data[i].toString().contains(searchText)) {numbers = i; break;}
		}
		
		return numbers;
	}
}

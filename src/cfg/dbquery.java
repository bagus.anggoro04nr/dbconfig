/**
 * 
 */
package cfg;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;

import documents.Create_Documents;
import libs.createnewfile;
import libs.funcs;
import libs.setgetcust;
import libs.setgetdate;
import libs.setgetpo;
import libs.typesnya;

/**
 * @author pangeran.jack@4nr
 *
 */
public class dbquery extends dbmysql{

	private String tables = "", resToFile="";
	private String params = "" ;
	private String whereparams = "";
	private String limits = "", typeResultNya="";
	private int conns;
	private String qry = "";
	private Object[] qryNya, qryNya1;
	private ResultSet resultSet;
	private int resultNya;
	private dbmysql objMySql = new dbmysql();
	private static PreparedStatement preStatement;
	private String[] tablesDatas, joinParamsDatas, whereParamse, resToFileList;
	private String orderParamsDatas = "", groupByParamsDatas = "";
	private Object hasilNya, resHasil, resToFileArray;
	private static List<Map<String, Object>> resHasil1;
	
	public Object dbquerystype(int conn, String param, String table, String whereparam, String typeresult) throws SQLException, Exception{
		params = "*";
		whereparams = "";
		
		if(!whereparam.toString().matches("null"))
			whereparams = "WHERE " + whereparam;
		
		if(!param.toString().matches("null") )
			params = param;
		
		tables = table;
		typeResultNya = typeresult;
		conns = conn;
		limits = "LIMIT 1";
		return qrySelect();
		
	}
	
	public Object dbquerysdata(int conn, String[] tablesData, String[] joinParamsData, String orderParamsData, String whereParam, String groupByParams, String querys, String querys1, String limitsnya, String params) throws SQLException, Exception{
		orderParamsDatas = "";
		whereparams = "";
		groupByParamsDatas = "";
		limits = "";

		if(!orderParamsData.toString().matches("null")) orderParamsDatas = " ORDER BY " + orderParamsData;
		
		whereParamse = whereParam.split("@");
		
		if(!whereParamse[0].toString().matches("null")) {
			
			if(typesnya.getType().equals("po")){
				
				if(setgetdate.getDateFrom().matches(".*\\d+.*") & setgetdate.getDateTo().matches(".*\\d+.*")){
					
					if(whereParamse.length > 1)
					whereparams = " WHERE " + whereParamse[0] + " '" + setgetdate.getDateFrom() + " 00:00:00' AND '" + setgetdate.getDateTo() + " 23:59:59' " +
							whereParamse[1] + "'" + setgetpo.getKodePO() + "' " + whereParamse[2] + "'" + setgetpo.getKodeItem() + "'";
					else whereparams = " WHERE " + whereParamse[0] + " '" + setgetdate.getDateFrom() + " 00:00:00' AND '" + setgetdate.getDateTo() + " 23:59:59' ";
					
//				}else if(setgetdate.getDateFrom().toString().contains("null") & setgetdate.getDateTo().toString().contains("null")){
//					
//					whereparams = " WHERE " + whereParamse[0] + " '" + setgetdate.giveDateNow() + " 00:00:00' AND '" + setgetdate.giveDateNow() + " 23:59:59' ";
//					
				}else whereparams = " WHERE " + whereParamse[0] ;
				
			}else if(typesnya.getType().equals("cust")){
				whereparams = " WHERE " + whereParamse[0] + " '" + setgetcust.getAutoID() + "' AND " + 
						whereParamse[1] + " '" + setgetcust.getKodeCust() + "' ";
			}
		}
		

		
		if(!groupByParams.toString().matches("null")) groupByParamsDatas = " GROUP BY " + groupByParams;
		
		if(!limitsnya.toString().matches("null")) limits = " LIMIT " + limitsnya;
		
		tablesDatas = tablesData[0].split(",");
		joinParamsDatas = joinParamsData[0].split(",");
		qryNya = querys.split("@");
		qryNya1 = querys1.split("@");
		conns = conn;

		if(params.matches("updatetable"))
			return qryUpdateData();
//		else if(params.matches("insert"))
//			return qryInsertData(params);
//		else if(params.matches("delete"))
//			return qryDeleteData(params);
		else return qrySelectData();
		
	}
	
	public Object qrySelect() throws Exception{

		qry = "SELECT " + params + " FROM " + tables + " " + whereparams + " " + limits + " ;";
//		System.out.println(qry);
		if(typeResultNya.contains("0")){
			hasilNya = toDB(qry, 0, 0, 9);
		}else if(typeResultNya.contains("1")){
			hasilNya = toDB(qry, 0, 1, 9);
		}else if(typeResultNya.contains("2")) {
			hasilNya = toDB(qry, 0, 2, 9);
		}
		return hasilNya;

	}
	
	public Object qrySelectData() throws SQLException, Exception{
		
		qry =  qryNya[0].toString() + tablesDatas[0].toString();
		
			for(int i=1; i < tablesDatas.length; i++){
				
			 qry += qryNya1[0].toString() + tablesDatas[i].toString()  + qryNya1[1]  +  joinParamsDatas[ i - 1] + qryNya1[2].toString();
			 
			}

		qry += whereparams;
		qry += groupByParamsDatas;
		qry += orderParamsDatas;
		qry += limits;
		qry +=  " ;";
//		System.out.println(qry);
//		System.exit(0);
		if(typesnya.getModalID().matches("reports"))
			if(typesnya.getTypeDoc().matches("0"))
			  return toDB(qry, 0, 3, 0);
			else return toDB(qry, 0, 3, 1);
		else return toDB(qry, 0, 0, 9);
	}
	
//	public String qryInsertData() throws SQLException, Exception{
//
//		qry = "INSERT INTO " + tables + " " + paramstablesnya + " VALUES(" + params + ");";
//		System.out.println(qry);
//		System.exit(0);
//		return toDB(qry, 1, 8, 9);
//	}
	
	public Object qryUpdateData() throws SQLException, Exception{
		if(typesnya.getType().equals("po")){
			qry = qryNya[0].toString() + tablesDatas[0].toString() 
					+ qryNya1[0].toString() + setgetpo.getNameCol() + " = '" + setgetpo.getValue() + "' " +
					whereparams ;
		}else if(typesnya.getType().equals("cust")){
			
			qry = qryNya[0].toString() + tablesDatas[0].toString() 
					+ qryNya1[0].toString() + setgetcust.getNameCol() + " = '" + setgetcust.getValue() + "' " +
					whereparams ;
		}
		qry +=  " ;";
//		System.out.println(qry);
//		System.exit(0);
		return toDB(qry, 1, 8, 8);
	}
	
//	public String qryDeleteData() throws SQLException, Exception{
//
//		qry = "DELETE FROM " + tables+ " " + whereparams + ";";
//		return toDB(qry, 1, 8);
//	}
	
	private Object toDB(String qrys, int execute, int TyResults, int typeDoc) throws SQLException, Exception{
//		System.out.println(conns + " - " + execute + " - " + TyResults);
		objMySql.dbmysqls(conns);
		preStatement = dbmysql.koneksi().prepareStatement(qrys);
		
		if(execute == 0 && TyResults == 0 && typeDoc == 9){
			
			resultSet = preStatement.executeQuery();
			resToFile = funcs.writeResultJSON(resultSet);
			resHasil = createnewfile.getNewFile(resToFile);
			
		}else if(execute == 0 && TyResults == 1 && typeDoc == 9){
			resultSet = preStatement.executeQuery();
			resToFileArray = funcs.writeResultArray(resultSet);
			resHasil = createnewfile.getNewFile(resToFileArray);
			
		}else if(execute == 0 && TyResults == 2 && typeDoc == 9){
			resultSet = preStatement.executeQuery();
			resHasil = funcs.resultDataList(resultSet);
			
		}else if(execute == 0 && TyResults == 3 && typeDoc == 0){
			
			resultSet = preStatement.executeQuery();
			resHasil1 = funcs.resultSetToArrayList(resultSet);
			resHasil = Create_Documents.createToPDF(resHasil1);
			
		}else if(execute == 0 && TyResults == 3 && typeDoc == 1){
			resultSet = preStatement.executeQuery();
			resHasil1 = funcs.resultSetToArrayList(resultSet);
			resHasil = Create_Documents.createToXLS(resHasil1);
			
		}else{
			resultNya = preStatement.executeUpdate();
			if(resultNya == 1)resHasil = " Berhasil !!!";
			else resHasil = " Tidak Berhasil !!!";
			
		}
	
		dbmysql.koneksi().close();
//		System.out.println(resHasil);
		return resHasil;
	}
		
}

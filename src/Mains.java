import java.sql.SQLException;

import libs.servers;
import libs.setgetcust;
import libs.setgetdate;
import libs.setgetpo;
import libs.typesnya;
import libs.users;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import cfg.dbtoquery;
import documents.Create_Documents;

/**
 * @author pangeran.jack@4nr
 *
 */
public class Mains{

	private static dbtoquery obj;
	private static String datanya, nilainya, nilaiitem;
	private static JSONObject jsonObject;
	private static JSONTokener tokener;
	private static String retrnItem = "", retrnDataCust="", retrnDataImg="";
	private static String retrnPO = "", resultsPO = "" , results = "";
	private static String resultLogin = "";
	private static int itemjmlh;
	private static String datefrom="";
	private static String dateto="";
	
	public static void main(String[] args) throws SQLException, Exception, JSONException  {

		datanya = args[0].replace("[","{").replace("]", "}");
//		System.out.println(datanya);
		tokener = new JSONTokener(datanya);
	
		jsonObject = new JSONObject(tokener);
		nilainya = String.valueOf(jsonObject.length() - 1);
		nilaiitem = String.valueOf(jsonObject.length() - 2);
//		System.out.println(jsonObject);
		obj = new dbtoquery();
//		System.exit(0);
		
		if(jsonObject.has("baseurls")){
		
		  servers.setHostname(jsonObject.get("baseurls").toString());
		
		  if(jsonObject.has("typesnya")){
			
			typesnya.setType(jsonObject.get("typesnya").toString());
			
//			System.out.println(servers.getHostname() + " hostname");
			if(typesnya.getType().contains("login")){
				users.setEmail(jsonObject.get("email").toString());
				users.setPassword(jsonObject.get("pass").toString());
				toQryLogin();
				
			}else if(typesnya.getType().matches("menu")){
				users.setEmail(jsonObject.get("email").toString());
				System.out.println(toQryMenu());
				
			}else if(typesnya.getType().matches("po")){
				typesnya.setModalID(jsonObject.get("modalid").toString());
				if(!jsonObject.get("datefrom").toString().contains("null") || !jsonObject.get("dateto").toString().contains("null")){
					setgetdate.setDateFrom(jsonObject.get("datefrom").toString());
					setgetdate.setDateTo(jsonObject.get("dateto").toString());
				}else{
					setgetdate.setDateFrom(setgetdate.giveDateNow().toString());
					setgetdate.setDateTo(setgetdate.giveDateNow().toString());
				}
				users.setEmail(jsonObject.get("email").toString());
				
				if(jsonObject.get("modalid").toString().matches("detail")){
					resultsPO = (String) dbtoquery.showTableData(obj, "po", "detail");
					
				}else if(jsonObject.get("modalid").toString().matches("edit")){
					
					resultsPO = (String) dbtoquery.showTableData(obj, "po", "edit");
				}else if(jsonObject.get("modalid").toString().matches("reports")){
					typesnya.setTypeDoc(jsonObject.get("typedoc").toString());
					resultsPO = (String) dbtoquery.showTableData(obj, "po", "reports");
					
				}else if(jsonObject.get("modalid").toString().matches("updatetable")){
					
					setgetpo.setKodeItem(jsonObject.get("kodeitem").toString()); 
					setgetpo.setValue(jsonObject.get("val").toString());
					setgetpo.setNameCol(jsonObject.get("dataid").toString());
					setgetpo.setKodePO(jsonObject.get("kodepo").toString());
					
					resultsPO = (String) dbtoquery.showTableData(obj, "po", "updatetable");
				}else resultsPO = (String) dbtoquery.showTableData(obj, "po", "showtable");
				System.out.println(resultsPO);
			
			}else if(typesnya.getType().equals("cust")){
				if(jsonObject.get("modalid").equals("updatetable")){
					setgetcust.setAutoID(jsonObject.get("kodepo").toString());
					setgetcust.setKodeCust(jsonObject.get("kodeitem").toString());
					setgetcust.setNameCol(jsonObject.get("dataid").toString());
					setgetcust.setValue(jsonObject.get("val").toString());
					
					results = (String) dbtoquery.showTableData(obj, "cust", "updatetable");
				}
				System.out.println(results);
			}
			
		}else		
		if(jsonObject.has(nilainya)){
		
			if(jsonObject.getJSONObject(nilainya).get("logs").equals("insert")){
				if(jsonObject.has(nilaiitem))
				itemjmlh = jsonObject.getJSONObject(nilaiitem).length();
//				System.out.println(jsonObject.getJSONObject(nilaiitem).getJSONObject("new"+0));
//				System.out.println("uyee insert");
				
				if(jsonObject.getJSONObject(nilainya).get("idnya").equals("po")){
						
					
//					if(jsonObject.getJSONObject(nilainya).get("for").equals("item")){
//						retrnItem = po.sendDataItem(obj, conns, itemjmlh, jsonObject, nilaiitem, nilainya);
//						
//					}else if(jsonObject.getJSONObject(nilainya).get("for").equals("cust")){
//						
//						retrnDataCust = po.sendDataCust(obj, conns, jsonObject, nilaiitem, nilainya);
//					}else if(jsonObject.getJSONObject(nilainya).get("for").equals("image")){
//						
//						retrnDataImg = po.sendDataImage(obj, conns, jsonObject, nilainya);
//					}
					
					retrnPO = retrnItem + " \r\n" + retrnDataCust + " \r\n" + retrnDataImg + " \r\n";
					
					System.out.println("PO : " + retrnPO);
				}
					
				
			}else if(jsonObject.getJSONObject(nilainya).get("logs").equals("select")){
				System.out.println("uyee select");
			}else if(jsonObject.getJSONObject(nilainya).get("logs").equals("update")){
				System.out.println("uyee update");
			}else if(jsonObject.getJSONObject(nilainya).get("logs").equals("delete")){
				System.out.println("uyee delete");
			}
		 }
		}
		System.exit(0);

	}
	

	private static void toQryLogin() throws SQLException, Exception{
	
		resultLogin = (String) dbtoquery.getLogin(obj);
		System.out.println(resultLogin);
		
	}
	
	private static String toQryMenu() throws SQLException, Exception{
		
		return (String) dbtoquery.showTableData(obj, "menu", "selectmenu");
		
	}

}

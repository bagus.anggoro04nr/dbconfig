/**
 * 
 */
package libs;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author ramad
 *
 */
public class setgetdate {
	
	private static String datefrom = "";
	private static String dateto = "";
	private static DateFormat dateFormat;
	private static Calendar cal;
	private static String dateNow="";
	
	public static void setDateFrom(String datefroms){
		datefrom = datefroms;
	}
	public static void setDateTo(String datetos){
		dateto = datetos;
	}
	
	public static String getDateFrom(){
		return datefrom;
	}
	public static String getDateTo(){
		return dateto;
	}
	
	public static String giveDateNow(){
		dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		cal = Calendar.getInstance();
		dateNow = dateFormat.format(cal.getTime()).toString();
		
		return dateNow;
	}
	
	public static String giveDateNowReport(){
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		cal = Calendar.getInstance();
		dateNow = dateFormat.format(cal.getTime()).toString();
		
		return dateNow;
	}
	
}

/**
 * 
 */
package libs;

import java.sql.SQLException;
import java.util.Arrays;

import org.json.JSONException;
import org.json.JSONObject;

import cfg.dbtoquery;
import cfg.readfile;

/**
 * @author ramad
 *
 */
public class po {

	private static String tablenya = "", jenisnya="";
	private static String params = "";
	private static String paramstable = "";
	private static String modes = "";
	private static String types = "";
	private static String whereparams = "";
	private static int limits;
	private static Object kodecreated="", kodepo="", kodebar="", namabar="", namabar1="", namabar2="", harbel="", jmlhbar="", fullBar="";
	private static Object retrns="";
	private static Object cust="", alcust="", tlp="", ctctcust="";
	private static Object images="";
	private static String[] tablesDatas, joinParamsDatas;
	private static String orderParamsDatas = "", groupByParamsDatas = "";
	private static String datapo = "";
	private static String[] vals;
	private static Object[] querys;
	
	
//	public static String sendDataItem(dbtoquery obj, int conns, int lengthItem, JSONObject json, String nilaiitem, String nilainya) throws Exception{
//		tablenya = "tbl_barang";
//		paramstable = "(kode_po, kode_barang, nama_barang, harga_beli, jmlh_barang, kode_created_by)";
//		System.out.println(lengthItem);
//		for (int i = 0; i < lengthItem; i++) {
//			
//			kodepo = json.get("new0");
//			kodebar = json.getJSONObject(nilaiitem).getJSONObject("new"+i).get("new1");
//			namabar = json.getJSONObject(nilaiitem).getJSONObject("new"+i).get("new2");
//			namabar1 = json.getJSONObject(nilaiitem).getJSONObject("new"+i).get("new3");
//			namabar2 = json.getJSONObject(nilaiitem).getJSONObject("new"+i).get("new4");
//			jmlhbar = json.getJSONObject(nilaiitem).getJSONObject("new"+i).get("new5");
//			harbel = json.getJSONObject(nilaiitem).getJSONObject("new"+i).get("new7");
//			kodecreated = json.getJSONObject(nilainya).get("kode");
//			
//			fullBar = kodebar + " " + namabar + " " + namabar1 + " " + namabar2;
//			
//			params = "'" + kodepo + "', '" + kodebar + "', '" + fullBar + "', '" + harbel + "', '" + jmlhbar + "', '" + kodecreated + "'";
//			
//			retrns = obj.qryInsert(jenisnya, conns, tablenya, paramstable, modes, typesnya, params, whereparams, limits);
//		}
//		
//		return (String) retrns;
//	}
	
//	public static String sendDataCust(dbtoquery obj, int conns, JSONObject json, String nilaiitem, String nilainya) throws Exception{
//		tablenya = "tbl_cust";
//		paramstable = "(customer, alamat_cust, no_tlp, contact_cust, kode_created_by)";
//		
////		kodecust = json.getJSONObject(nilaiitem).get("new0");
//		cust = json.get("new1");
//		alcust = json.get("new5");
//		tlp = json.get("new10");
//		ctctcust = json.get("new13");
//		kodecreated = json.getJSONObject(nilainya).get("kode");
//		
//		params = "'" + cust + "', '" + alcust + "', '" + tlp + "', '" + ctctcust + "', '" + kodecreated + "'";
//		
//		retrns = obj.qryInsert(jenisnya, conns, tablenya, paramstable, modes, typesnya, params, whereparams, limits);
//		return (String) retrns;
//	}
	
//	public static String sendDataImage(dbtoquery obj, int conns, JSONObject json, String nilainya) throws Exception{
//		tablenya = "tbl_img_fakturpemb";
//		paramstable = "(img, kode_created_by)";
//		
//		images = readfile.readFileImages();
//		kodecreated = json.getJSONObject(nilainya).get("kode");
//		
//		params = "'" + images + "', '" + kodecreated + "'";
//		
//		retrns = obj.qryInsert(jenisnya, conns, tablenya, paramstable, modes, typesnya, params, whereparams, limits);
//		return (String) retrns;
//	}
	
	private static String[] fromFilePO() throws Exception{
		
		datapo = readfile.readFilePO();
		return datapo.split("@@");
		
	}
	

	
//	public static String showEditDataPO(dbtoquery obj, int conns, String kodepo) throws SQLException, Exception{
//		vals = fromFilePO();
//		querys = obj.dbquerystype(0, vals[0], vals[1], vals[3], vals[6]);
//	}
//	
//	public static String showDetailDataPO(dbtoquery obj, int conns, String kodepo) throws SQLException, Exception{
//		vals = fromFilePO();
//		querys = obj.dbquerystype(0, vals[0], vals[1], vals[4], vals[6]);
//	}
//	
//	public static String showReportDataPO(dbtoquery obj, int conns, String kodepo) throws SQLException, Exception{
//		vals = fromFilePO();
//		querys = obj.dbquerystype(0, vals[0], vals[1], vals[5], vals[6]);
//	}
	
//	public static String getDataPO(dbtoquery obj, int conns, String kodepo) throws SQLException, Exception{
//		
//		vals = fromFilePO();
//		querys = obj.dbquerystype(0, vals[0], vals[1], vals[2]);
//		
//		
//		
//		tablesDatas = new String[]{"tbl_po", "tbl_cust", "tbl_barang", "tbl_img_fakturpemb"};
//		joinParamsDatas = new String[]{"tbl_po.kode_cust = tbl_cust.kode_cust", "tbl_po.kode_barang = tbl_barang.kode_barang", "tbl_po.kode_po = tbl_img_fakturpemb.kode_po"};
//		orderParamsDatas = " tbl_po.autoid_po ";
//		groupByParamsDatas = " tbl_barang.nama_barang ";
//		if(kodepo.length() > 0)
//		whereparams = "  tbl_po.kode_po = '" + kodepo + "'  " ;
//		else whereparams = "";
//		
//		retrns = obj.dbquerysdata(conns, tablesDatas, joinParamsDatas, orderParamsDatas, whereparams, groupByParamsDatas);
//		return (String) retrns;
//	}
}
